/**
* @module layout - static design
*/

/**
* @function head - we create the link element inside head and assign it a favicon
* @param {Object} json - favicon
*/
function head(json){
    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    link.rel="shortcut icon";
    link.type="image/png";
    link.href=json.logo;
    head.appendChild(link);
}
/**
* @function header - we assign a logo in the menu
* @param {Object} json - logotip and name
*/
function header(json){
    document.getElementById("header_logo").src = json.logo;
    document.getElementById("header_logo").alt = json.name;
    document.getElementById("header_logo").height = 44;
    document.getElementById("header_logo").width = 44;
    
}
/**
* @function footer - assign company data in the footer
* @param {Object} json - data -> name, city, zipcode, phone, facebook and twitter
*/
function footer(json){
    document.getElementsByClassName('footer-item__company')[0].innerHTML = json.name;
    document.getElementsByClassName('footer-item__city')[0].innerHTML = json.city;
    document.getElementsByClassName('footer-item__zipCode')[0].innerHTML = json.zipcode;
    document.getElementsByClassName('footer-item__phone')[0].innerHTML = json.phone;
    document.getElementsByClassName('footer-social__facebook')[0].href = json.facebook;
    document.getElementsByClassName('footer-social__twitter')[0].href = json.twitter;
}

export {head,header,footer};