import {toaster} from '../../utils/utils';
/**
* @function validateContact - get values and call validate to validate values contact
*/
function validateContact(){
  console.log("hiiiiiiiiiiiii");
  let name = document.getElementById('name').value;
  let email = document.getElementById('email').value;
  let subject = document.getElementById('subject').value;
  let comment = document.getElementById('comment').value;
  let rdo = validate(name,email,subject,comment);
  //console.log("val:",val);
  if(rdo==true){
    console.log("ja es pot enviar al server");
    return rdo;
    //enviar datos
    //postAjax('http://127.0.0.1:8000/contacto/', { nombre: 1, telefono: 'Hello World',email:'joanmodaw@gmail.com',descripcion:'dsds' }, function(data){ console.log(data); });
  }
  return rdo;   
}
/**
* @function validate - validate values with patterns and check if value is empty
*/
function validate(name,email,subject,comment){
  var rdo = true;
  errorName.innerHTML = "";
  errorName.className = "";
  errorEmail.innerHTML = "";
  errorEmail.className = "";
  errorSubject.innerHTML = "";
  errorSubject.className = "";
  errorComment.innerHTML = "";
  errorComment.className = "";
  /* patterns */
  let nameReg = /^[A-Za-z\sñÑ]{4,10}$/;
  let emailReg = /^[A-Za-z0-9ñÑ]{4,15}[@]{1}[A-Za-z]{3,12}[.]{1}[A-Za-z]{2,5}$/;
  let commentReg = /^[A-Za-z\sñÑ]{4,35}$/;
  if(name.length==0 || name == null){
    rdo = false;
    let errorName = document.getElementById('errorName');
    toaster(errorName,"errors","El nombre es requerido");
    return false;
  }

  if(email.length==0 || email==null){
    rdo = false;
    let errorEmail = document.getElementById('errorEmail');
    toaster(errorEmail,"errors","El email es requerido");
    return false;
  }

  if(subject.length==0 || subject==null){
    rdo = false;
    let errorSubject = document.getElementById('errorSubject');
    toaster(errorSubject,"errors","El subject es requerido");
    return false;
  }

  if(comment.length==0 || comment==null){
    rdo = false;
    let errorComment = document.getElementById('errorComment');
    toaster(errorComment,"errors","El commentario es requerido");
    return false;
  }
  /*  patterns */
  if(!nameReg.test(name)){
    rdo = false;
    let errorName = document.getElementById('errorName');
    toaster(errorName,"errors","El nombre permite solo letras de 4-10 caracteres");
    return false;
  }

  if(!emailReg.test(email)){
    rdo = false;
    let errorEmail = document.getElementById('errorEmail');
    toaster(errorEmail,"errors","El email tiene que ser valido");
    return false;
  }

  if(!nameReg.test(subject)){
    rdo = false;
    let errorSubject = document.getElementById('errorSubject');
    toaster(errorSubject,"errors","El subject permite solo letras de 4-10 caracteres");
    return false;
  }

  if(!commentReg.test(comment)){
    rdo = false;
    let errorComment = document.getElementById('errorComment');
    toaster(errorComment,"errors","El comentario permite solo letras de 4-35 caracteres");
    return false;
  }

  return rdo;
}

export {validate,validateContact};