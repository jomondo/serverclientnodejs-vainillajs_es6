import ControlCtrl from "../Control/controlCtrl";
import { validateContact } from './validateContact';
import { cambiarIdioma } from '../../lib/language';
/**
 * @class
 * @extends ControlCtrl
 */
class ContactCtrl extends ControlCtrl {
  /**
     * @constructor
     * @param {string} id - id value.
     * @param {string} template - template value.
     * @param {Object} data - data empresa value.
     */
  constructor(id, template, data) {
    console.log("entra en constructor ContactCtrl");
    super(id, template, data);
    this.createGoogleMaps();
    this.eventValidate();
    this.changeLanguageActived();
  }
  /**
     * @method createGoogleMaps - create google maps on head
     */
  createGoogleMaps() {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyAqROLBq6gjC6c2PDeJdPCluf_oj-ujy3c&callback=initMap";
    script.async;
    script.defer;
    head.appendChild(script);
  }
  /**
     * @method eventValidate - when click submitContact, validateContact is activated to validate data
     */
  eventValidate() {
    /* when click on the button submit, we activate contact validation*/
    document.getElementById('submitContact').addEventListener("click", validateContact);
  }
  /**
     * @method changeLanguageActived - when start contact, cambiarIdioma is activated 
     * to change data from the predefined or selected language
     */
  changeLanguageActived() {
    cambiarIdioma();
  }
}
export default ContactCtrl;