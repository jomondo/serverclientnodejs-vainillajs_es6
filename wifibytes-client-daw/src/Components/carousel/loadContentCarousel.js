import { templateJumbotrons } from '../../templates/home';
/* based on -> https://codepen.io/AaronCDR/pen/XbXZay */

/**
* @function carousel - generate template, set, hide, show carousel 
*/
function carousel(response) {
    /*console.log("entra",response);
    console.log("entra en functin carousel");*/
    var carousel = document.getElementById('carousel');
    var slides = 3;
    var speed = 7000; // 5 seconds
    /**
    * @param {element} carousel - get element carousel by id
    * @param {Number} slides - set 3 sliders
    * @param {Number} speed - speed when the slide change
    * @param {Number} num - position array jumbotron
    * @function carouselHide - set atributte data-state to hide carousel 
    */
    function carouselHide(num) {
        indicators[num].setAttribute('data-state', '');
        slides[num].setAttribute('data-state', '');
        slides[num].style.opacity = 0;
    }
    /**
    * @function carouselShow - set atributte active to show carousel
    */
    function carouselShow(num) {
        indicators[num].checked = true;
        indicators[num].setAttribute('data-state', 'active');
        slides[num].setAttribute('data-state', 'active');
        slides[num].style.opacity = 1;
    }
    /**
    * @function setSlide - reset all slides, set defined slide as active 
    * and Stop the auto-switcher with clearInterval
    */
    function setSlide(slide) {
        return function () {
            indicators.forEach(function (element, index, array) {
                array[index].setAttribute('data-state', '');
                slides[index].setAttribute('data-state', '');
                carouselHide(index);
            });

            indicators[slide].setAttribute('data-state', 'active');
            slides[slide].setAttribute('data-state', 'active');
            carouselShow(slide);

            clearInterval(switcher);
        };
    }
    /**
    * @function switchSlide - reset all slides, remove all active states & hide 
    * and set next slide as active & show the next slide
    */
    function switchSlide() {
        var nextSlide = 0;
        indicators.forEach(function (element, index, array) {
            if ((array[index].getAttribute('data-state') == 'active') && (index !== (array.length - 1))) nextSlide = index + 1;

            carouselHide(index);
        });

        carouselShow(nextSlide);
    }

    if (carousel) {
        console.log("entra en if caousel");
        //create template carousel that is hidden and shown
        var slides, switcher, indicators;
        /* We use is pattern to search by key all the jumbotrons 
        *that start with jumbotron- 
        */
        let nameReg = /^jumbotron-[0-9]{1}$/;
        //let json = JSON.parse(response);
        var jumbo = document.getElementById('jumbotron');
        var jumboText = document.getElementById('jumbotron-text');

        let jumbotrons = response.textos.filter(jumbo => nameReg.test(jumbo.key));
        console.log("jumbotrons",jumbotrons);
        jumbotrons.map((value) => {
            let data = {
                id: "jumbotron",
                text: value.content,
                media: value.image
            };
            let template = templateJumbotrons(data);
            jumbo.innerHTML += template;
            console.log("text", data.text);
            jumboText.innerHTML = data.text;
        });

        slides = carousel.querySelectorAll('.jumbotron');
        indicators = carousel.querySelectorAll('.indicator');
        switcher = setInterval(function () {
            switchSlide();
        }, speed);
        indicators.forEach(function (element, index, array) {
            array[index].addEventListener("click", setSlide(index));
        });

    }

}

export { carousel };