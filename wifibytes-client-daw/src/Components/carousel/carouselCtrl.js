import ControlCtrl from "../Control/controlCtrl";
import {carousel} from "./loadContentCarousel";
/**
 * @class
 * @extends ControlCtrl
 */

class CarouselCtrl extends ControlCtrl {
    /**
     * @constructor
     * @param {string} id - id value.
     * @param {string} template - template value.
     * @param {Object} data - content value.
     */

    constructor(id,template,data) {
        console.log("entra en constructor carousel");
        super(id,template);
        this.contentCarousel(data);   
    }
    /**
     * @method contentCarousel - show content carousel
     */
    contentCarousel(data){
        carousel(data);
    }
   
}
export default CarouselCtrl;