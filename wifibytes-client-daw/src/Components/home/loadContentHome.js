import {
    templateTarifasHome,
    templateTarifasHomeSinSubcategorias,
    templateInsideTarifasHome
} from '../../templates';
/**
* @function contentHome -  we filter the highlighted rates and inject them into the home
*/
function contentHome(responseTarifas) {
    console.log("inside-contentHome");
    let content = document.getElementById("body");
    console.log("content", content);
    /* we look for outstanding rates */
    let destacado = responseTarifas.results.filter(tarifas => tarifas.destacado == true);
    /* we injected outstanding rates */
    destacado.map((element) => {
        /* we look tarifas that it have subtarifas to paint a different view */
        let sub = element.subtarifas.filter(subtar => subtar);
        //console.log("sub",sub);
        if (sub.length >= 1) {
            const dataTarifas = {
                nameTarifa: element.nombretarifa,
                precioTarifa: element.precio,
                logoTarifa: element.logo,
                subtarifas: element.subtarifas[0].subtarifa_datos_internet
            };
            //templateTarifasHome
            let templateTarifas = templateTarifasHome();
            content.innerHTML += templateTarifas;

            let contentTarifas = document.getElementById("tarifas");

            let templateInsideTarifas = templateInsideTarifasHome(dataTarifas);
            contentTarifas.innerHTML += templateInsideTarifas;
        } else {
            const dataTarifas = {
                nameTarifa: element.nombretarifa,
                precioTarifa: element.precio,
                logoTarifa: element.logo,

            };
            let contentTarifas = document.getElementById("tarifas");
            let templateTarifas = templateTarifasHomeSinSubcategorias(dataTarifas);
            contentTarifas.innerHTML += templateTarifas;
        }

    });

}

export {contentHome};
