import ControlCtrl from "../Control/controlCtrl";
import {contentHome} from './loadContentHome';
/**
 * @class
 * @extends ControlCtrl
 */
class HomeCtrl extends ControlCtrl{
    /**
     * @constructor
     * @param {string} id - id value.
     * @param {string} template - template value.
     * @param {Object} responseHome - data home value.
     * @param {Object} responseTarifas - data rates value.
     */
    constructor(id,template,responseHome,responseTarifas) {
        console.log("entra en constructor HomeCtrl");
        super(id,template,responseHome);
        this.content(responseTarifas);
    }
    /**
     * @method content - inyected tarifas on home
     */
    content(responseTarifas){
        contentHome(responseTarifas);
    }
}
export default HomeCtrl;