import { templateSmartphones, templateCategories, templateFilters } from '../../templates';
import { mountTemplate } from "./utils";
/**
* @function filterCategories - filter and show articles by category
*/
function filterCategories(response, cod) {
    var elementCatalogue = document.getElementById("container-catalogue");
    elementCatalogue.innerHTML = "";
    let templateFilter = templateFilters();
    elementCatalogue.innerHTML += templateFilter;
    console.log("responseCatalogue", response);
    let dataCatalogue = response.filter(catalogue => catalogue.codfamilia == cod);
    console.log("dataCataloge", dataCatalogue);
    dataCatalogue.map((element) => {
        console.log("element cataloge", element.descripcion);
        var contentData = {
            name: element.descripcion,
            media: element.imagen,
            price: element.pvp
        };
        let templ = templateSmartphones(contentData);
        elementCatalogue.innerHTML += templ;
    });
}
/**
* @function contentCatalogue - call filterCategories and filter by ram and price
*/
function contentCatalogue(response, cod) {
    var arr = [];
    filterCategories(response, cod);
    function filters() {
        let elementCat = document.getElementById("container-catalogue");
        let rams = document.getElementById('articuls-filters__Ram').value;
        let brand = document.getElementById('articuls-filters__brand').value;
        let all = document.getElementById('articuls-filters__all').value;
        console.log("all", all);
        var key = rams || brand || all;
        console.log("key", key);
        switch (key) {
            case rams:
                var dataCatalogue = response.filter(catalogue => catalogue.codfamilia == cod && catalogue.ram == rams);
                console.log("dataFilter", dataCatalogue);
                arr.push(dataCatalogue);
                break;

            case brand:
                console.log("arr", arr[0]);
                console.log("brand", brand);
                if (arr[0] == undefined) {
                    var dataCatalogue = response.filter(catalogue => catalogue.codfamilia == cod
                        && catalogue.marca == brand);
                    console.log("dataFilterBrandff", dataCatalogue);
                } else {
                    var dataCatalogue = arr[0].filter(catalogue => catalogue.codfamilia == cod
                        && catalogue.marca == brand);
                    console.log("dataFilterBrand", dataCatalogue);
                }
                break;
            case all:
                console.log("entra all");
                var dataCatalogue = response.filter(catalogue => catalogue.codfamilia == cod);
                console.log("dataFilterBrandff", dataCatalogue);
                arr[0] = undefined;
                break;
        }

        mountTemplate(elementCat, dataCatalogue);
        document.getElementById('articuls-filters__brand').addEventListener("change", filters);
        document.getElementById('articuls-filters__Ram').addEventListener("change", filters);
        document.getElementById('articuls-filters__all').addEventListener("change", filters);
    }
    document.getElementById('articuls-filters__brand').addEventListener("change", filters);
    document.getElementById('articuls-filters__Ram').addEventListener("change", filters);
    document.getElementById('articuls-filters__all').addEventListener("change", filters);

}
/**
* @function contentCategories - show categories (smartphone and tablets)
*/
function contentCategories(response) {
    let elementCatalogue = document.getElementById("container-catalogue");
    console.log("responseCatalogue", response);
    response.results.map((element) => {
        console.log("el cat", element.nombre);
        let contentData = {
            name: element.nombre,
            media: element.icono
        };
        elementCatalogue.innerHTML += templateCategories(contentData);
    });
}

export { contentCatalogue, contentCategories };