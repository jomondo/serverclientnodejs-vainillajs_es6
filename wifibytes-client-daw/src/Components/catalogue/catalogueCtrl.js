import ControlCtrl from "../Control/controlCtrl";
import { contentCatalogue, contentCategories } from "./loadContentCatalogue";

/**
 * @class
 * @extends ControlCtrl
 */
class CatalogueCtrl extends ControlCtrl {
    /**
     * @constructor
     * @param {string} id - id value.
     * @param {string} template - template value.
     * @param {Object} data1 - articles value.
     * @param {Object} data2 - categories value.
     */
    constructor(id, template, data1, data2) {
        console.log("entra en constructor catalogue");
        super(id, template);
        this.categories(data2);
        this.content(data1);
    }
    /**
     * @method content - show articles (smartphones and tablets)
     */
    content(data1) {
        function detailSmartphone() {
            contentCatalogue(data1,"#S1");
        }

        function detailTablet() {
            contentCatalogue(data1,"#T1");
        }
        
        document.getElementById('btn-list__Smartphones').addEventListener("click", detailSmartphone);
        document.getElementById('btn-list__Tablets').addEventListener("click", detailTablet);

    }
    /**
     * @method categories - show categories
     */
    categories(data2) {
        contentCategories(data2);
    }

}
export default CatalogueCtrl;