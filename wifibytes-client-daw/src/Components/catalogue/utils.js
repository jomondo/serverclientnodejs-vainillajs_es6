import {templateSmartphones,templateFilters} from '../../templates';
/**
* @function mountTemplate - show articles filtered
*/
function mountTemplate(elementCat,dataCatalogue) {
    elementCat.innerHTML = "";
    let templateFilter = templateFilters();
    elementCat.innerHTML += templateFilter;
    dataCatalogue.map((element) => {
        console.log("element cataloge", element.descripcion);
        let content = {
            name: element.descripcion,
            media: element.imagen,
            price: element.pvp
        };
        let templ = templateSmartphones(content);
        elementCat.innerHTML += templ;
    });
}


export { mountTemplate };