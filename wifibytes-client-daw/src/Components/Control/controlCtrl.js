import {getTemplate} from "../../templates/index";
/**
 * @class
 */
class ControlCtrl {
    /**
     * @constructor
     * @param {string} id - id value.
     * @param {string} template - template value.
     * @param {Object} data - values.
     */
    constructor(id,template,data) {
        console.log("entra en constructor control jeje 2");
        this.render(id,template,data);   
    }
    /**
     * @method render - call getTemplate to inject the template
     */
    render(id,template,data){
        console.log("entra en render control",id , template);
        getTemplate(id,template,data);
    }
}
export default ControlCtrl;