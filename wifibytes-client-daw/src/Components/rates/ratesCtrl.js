import ControlCtrl from "../Control/controlCtrl";
import {contentRates} from "./loadContentRates";
/**
 * @class
 * @extends ControlCtrl
 */
class RatesCtrl extends ControlCtrl {
    /**
     * @constructor
     * @param {string} id - id value.
     * @param {string} template - template value.
     * @param {Object} data - rates value.
     */
    constructor(id,template,data) {
        super(id,template);
        this.contentRates(data);   
    }
    /**
     * @method contentRates - show content rates
     */
    contentRates(data){
        contentRates(data);
    }
   
}
export default RatesCtrl;