import { templateAllRates, templateRatesSinSubcategorias } from "../../templates/rates";
/**
 * @function contentRates - inyect rates on template rates
 * @param {Object} data - rate values
 */
function contentRates(data) {
    console.log("tarifas", data.results);
    let rates = document.getElementById("tarifas");

    data.results.map((element) => {
        if (element.subtarifas.length == 0) {
            const dataTarifas = {
                nameTarifa: element.nombretarifa,
                precioTarifa: element.precio,
                logoTarifa: element.logo
            };
            let templateRates = templateRatesSinSubcategorias(dataTarifas);
            rates.innerHTML += templateRates;
        } else {
            const dataTarifas = {
                nameTarifa: element.nombretarifa,
                precioTarifa: element.precio,
                logoTarifa: element.logo,
                subtarifas: element.subtarifas[0].subtarifa_datos_internet
            };
            let templateRates = templateAllRates(dataTarifas);
            rates.innerHTML += templateRates;
        }



    });
}

export { contentRates };