import {generaTemplate} from '../utils/utils';

function templateHome(data){
    const form = generaTemplate`
        <h1 class="${'classHomeTitle'} font-nice__title">${'title'}</h1>
        <h3 class="home-subtitle font-nice__title">${'subtitle'}</h3>
        <section class="container-home">
            <div class="home-box">
                <h3 class="home-box__title font-nice__title">${'titleLeft'}</h3>
                <div class="font-nice__content" align="justify">${'boxLeft'}</div>
            </div>
            <div class="home-box">
                <h3 class="home-box__title font-nice__title">${'titleRight'}</h3>
                <div class="font-nice__content" align="justify">${'boxright'}</div>
            </div>
        </section>
        <h3 align="center">Nuestras mejores tarifas</h3>`(data);
    
    return form;

}

function templateInsideTarifasHome(data){
    const form = generaTemplate`
        <div class="home-tarifas">
            <div align="center" class="container-tarifas__characteristics space-top">${'nameTarifa'}</div>
            <div align="center" class="space-top"><img src="${'logoTarifa'}" height="150" with="150"/></div>
            <div class="home-tarifas__price space-top">${'precioTarifa'} €/mes</div>
            <div class="container-tarifas__characteristics space-top">${'subtarifas'} mb/s de bajada</div>
            <div align="center"><button class="btn space-top">Detail</button></div>
            
        </div>
    `(data);
    return form;
}

function templateTarifasHome(data){
    const form = generaTemplate`
    <section id="tarifas" class="container-home__tarifas"></section>`(data);
    return form;
}

function templateTarifasHomeSinSubcategorias(data){
    const form = generaTemplate`
    <div class="home-tarifas">
        <div align="center" class="container-tarifas__characteristics space-top">${'nameTarifa'}</div>
        <div align="center" class="space-top"><img src="${'logoTarifa'}" height="150" with="150"/></div>
        <div class="home-tarifas__price space-top">${'precioTarifa'} €/mes</div>
        <div align="center"><button class="btn space-top">Detail</button></div>
</div>
    `(data);
    return form;
}

function templateCarousel(data){
    const form = generaTemplate`
    <div id="jumbotron" class="jumbotrons"></div>
        <div id="indicators" class="indicators">
          <div id="jumbotron-text" class="jumbotron-text"></div>
          <input class="indicator" name="indicator" data-slide="1" data-state="active" checked type="radio" />
          <input class="indicator" name="indicator" data-slide="2" type="radio" />
          <input class="indicator" name="indicator" data-slide="3" type="radio" />
        </div>`(data);
    return form;
}

function templateJumbotrons(data){
    const form = generaTemplate`
    <div>
        <img class="${'id'}" src="${'media'}">
    </div>`(data);
    return form;
}

export {
    templateHome,
    templateTarifasHome,
    templateCarousel,
    templateTarifasHomeSinSubcategorias,
    templateInsideTarifasHome,
    templateJumbotrons
};