import {generaTemplate} from '../utils/utils';

function templateTarifas(data){
    const form = generaTemplate`
    <section id="tarifas" class="container-home__tarifas"></section>`(data);
    return form;
}

function templateAllRates(data){
    const form = generaTemplate`
    <div class="home-tarifas">
        <div align="center" class="container-tarifas__characteristics space-top">${'nameTarifa'}</div>
        <div align="center" class="space-top"><img src="${'logoTarifa'}" height="150" with="150"/></div>
        <div class="home-tarifas__price space-top">${'precioTarifa'} €/mes</div>
        <div class="container-tarifas__characteristics space-top">${'subtarifas'} mb/s de bajada</div>
        <div align="center"><button class="btn space-top">Detail</button></div>
    </div>
    `(data);
    return form;
}

function templateRatesSinSubcategorias(data){
    const form = generaTemplate`
    <div class="home-tarifas">
        <div align="center" class="container-tarifas__characteristics space-top">${'nameTarifa'}</div>
        <div align="center" class="space-top"><img src="${'logoTarifa'}" height="150" with="150"/></div>
        <div class="home-tarifas__price space-top">${'precioTarifa'} €/mes</div>
        <div align="center"><button class="btn space-top">Detail</button></div>
        
    </div>
    `(data);
    return form;
}

export{templateTarifas,templateAllRates,templateRatesSinSubcategorias};