import { templateHome, templateTarifasHome, templateTarifasHomeSinSubcategorias, templateCarousel,templateInsideTarifasHome } from '../templates/home';
import { templateCookies } from '../templates/cookies';
import { templateAvisoLegal } from '../templates/avisoLegal';
import { templateAbout } from '../templates/about';
import { templateCatalogue, templateSmartphones, templateCategories, templateFilters } from '../templates/catalogue';
import { templateContact } from '../templates/contact';
import { templateTarifas, templateRatesSinSubcategorias } from '../templates/rates';
/**
* @module templates - views of all app
*/
/**
 * @param {Object} data - data that contains id or content to add on template
* @function templateAbout - generate template about
*/
/**
 * @param {Object} data - data that contains id or content to add on template
* @function templateHome - generate template home
*/
/**
 * @param {Object} data - data that contains id or content to add on template
* @function templateTarifasHome - generate template tarifas home
*/
/**
 * @param {Object} data - data that contains id or content to add on template
* @function templateTarifasHomeSinSubcategorias - generate template sinSubcategoriastarifas home
*/
/**
 * @param {Object} data - data that contains id or content to add on template
* @function templateCarousel - generate template carousel home
*/
/**
 * @param {Object} data - data that contains id or content to add on template
* @function templateCatalogue - generate template catalogue
*/
/**
 * @param {Object} data - data that contains id or content to add on template
* @function templateSmartphones - generate template smartphones
*/
/**
 * @param {Object} data - data that contains id or content to add on template
* @function templateCategories - generate template categories
*/
/**
 * @param {Object} data - data that contains id or content to add on template
* @function templateCookies - generate template cookies
*/
/**
 * @param {Object} data - data that contains id or content to add on template
* @function templateAvisoLegal - generate template aviso legal
*/

/**
 * @param {Object} data - data that contains id or content to add on template
* @function getTemplate - inject the template with innerHTML by id while it exists
*/
function getTemplate(id, template, data) {
    switch (template) {
        case "catalogue":
            let contentTemplate = templateCatalogue();
            try {
                let element = document.getElementById(id);
                if (document.body.contains(element)) return element.innerHTML = contentTemplate;
                else console.error("Ei developer! it has one error for element " + id + " doesn't exist");
            } catch (error) {
                if (document.body.contains(element)) return element.innerHTML = "Problems rendering with the component" + template + " -> " + error;
                throw error;
            }
            break;
        case "home":
            const contentData = {
                classHomeTitle: "home-title",
                title: data[0].titulo,
                subtitle: data[0].subtitulo,
                titleLeft: data[0].caja_izquierda_titulo,
                boxLeft: data[0].caja_izquierda_texto,
                titleRight: data[0].caja_derecha_titulo,
                boxright: data[0].caja_derecha_texto,
            };
            let home = templateHome(contentData);
            try {
                let content = document.getElementById(id);
                content.innerHTML = "";//we empty content
                if (document.body.contains(content)) return content.innerHTML = home;
                else console.error("Ei developer! it has one error for element " + id + " doesn't exist");
            } catch (error) {
                if (document.body.contains(content)) return content.innerHTML = "Problems rendering with the component" + template + " -> " + error;
                throw error;
            }
            break;
        case "carousel":
            let contentCarousel = templateCarousel();
            try {
                let elementCarousel = document.getElementById(id);
                if (document.body.contains(elementCarousel)) return elementCarousel.innerHTML = contentCarousel;
                else console.error("Ei developer! it has one error for element " + id + " doesn't exist");
            } catch (error) {
                if (document.body.contains(elementCarousel)) return elementCarousel.innerHTML = "Problems rendering with the component" + template + " -> " + error;
                throw error;
            }
            break;
        case "rates":
            let contentRates = templateTarifas();
            try {
                let elementRates = document.getElementById(id);
                if (document.body.contains(elementRates)) return elementRates.innerHTML = contentRates;
                else console.error("Ei developer! it has one error for element " + id + " doesn't exist");
            } catch (error) {
                if (document.body.contains(elementRates)) return elementRates.innerHTML = "Problems rendering with the component" + template + " -> " + error;
                throw error;
            }
            break;
        case "contact":
            const dataContact = {
                nameText: "name",
                typeText: "text",
                email: "email",
                subjectText: "subject",
                commentText: "comment",
                idForm: "form-contact",
                name: data.name,
                phone: data.phone,
                city: data.city,
                province: data.province,
                country: data.country
            };
            let contentContact = templateContact(dataContact);
            try {
                let elementContact = document.getElementById(id);
                if (document.body.contains(elementContact)) return elementContact.innerHTML = contentContact;
                else console.error("Ei developer! it has one error for element " + id + " doesn't exist");
            } catch (error) {
                if (document.body.contains(elementContact)) return elementContact.innerHTML = "Problems rendering with the component" + template + " -> " + error;
                throw error;
            }
            break;
    }
}
export {
    templateHome,
    getTemplate,
    templateTarifasHome,
    templateTarifasHomeSinSubcategorias,
    templateInsideTarifasHome,
    templateCookies,
    templateAvisoLegal,
    templateAbout,
    templateCatalogue,
    templateSmartphones,
    templateCategories,
    templateFilters,
    templateTarifas,
    templateRatesSinSubcategorias
};