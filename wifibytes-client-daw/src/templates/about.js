import {generaTemplate} from '../utils/utils';

function templateAbout(data){
    const contentAbout = generaTemplate`
        <section class="about" id="${'id'}"><h4>Sobre nosotros</h4>${'content'}</section>`(data);
    return contentAbout;

}

export {templateAbout};