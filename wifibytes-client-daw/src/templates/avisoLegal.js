import {generaTemplate} from '../utils/utils';

function templateAvisoLegal(data){
    const contentAvisoLegal = generaTemplate`
    <section class="avisoLegal" id="${'id'}"><h4>Aviso Legal</h4>${'content'}</section>`(data);
    //console.log("estic en temlate: ",form);

    return contentAvisoLegal;

}

export {templateAvisoLegal};