import {generaTemplate} from '../utils/utils';
//<div id="container-articuls"></div>
function templateCatalogue(data){
    const contentCatalogue = generaTemplate`
        <h4 align="center">Catalogue</h4>
        <section id="container-catalogue" class="container-catalogue">
            <div class="categories"></div>
            <div class="filters" id="filters"></div>
        </section>`(data);
    return contentCatalogue;

}

function templateCategories(data){
    const contentCategories = generaTemplate`
        <div class="categories-item" id="${'name'}" align="center">
            <img src="${'media'}" alt="category" with="100" height="100"><br>
            <button id="btn-list__${'name'}" class="${'name'} btn">Ver</button>
        </div>`(data);
    return contentCategories;

}

function templateFilters() {
    const contentFilters = generaTemplate`
    <div class="articuls-filters">
        <select id="articuls-filters__Ram">
            <option id="" value="">Ram</option>
            <option id="" value="1">3</option>
            <option id="" value="2">6</option>
        </select>
        <select id="articuls-filters__brand">
            <option id="" value="">Brand</option>
            <option id="" value="1">BQ</option>
            <option id="" value="2">Samsung</option>
            <option id="" value="3">Xiaomi</option>
            <option id="" value="4">Iphone</option>
            <option id="" value="5">Asus</option>
        </select>
        <select id="articuls-filters__all">
            <option id="" value="">Reset</option>
            <option id="" value="all">All</option>
        </select>
    </div>
        `();
    return contentFilters;
}

function templateSmartphones(data){
    const content = generaTemplate`
    <div class="articuls-item space-top" align="center">
        <div class="space-top">${'name'}</div>
        <img class="space-top" src="${'media'}" alt="${'name'}" with="150" height="150">
        <div class="space-top">${'price'}€</div>
        <button class="btn space-top">Detail</button>
    </div>
        `(data);
    return content;

}

export {templateCatalogue,templateSmartphones,templateCategories,templateFilters};