import { generaTemplate } from '../utils/utils';

function templateContact(data) {

  const form = generaTemplate`
    <section class="container-contact">
      <form id="${'idForm'}">
        <label><strong data-tr="Get in contact with us"></strong></label><br><br>
        <div class="form-inputs">
          <label for="name" data-tr="Name"></label><br>
          <input class="input-beautiful input-focus--active" placeholder="Su nombre" type="${'typeText'}" id="${'nameText'}" pattern="[A-Za-z\sñÑ]{4,10}" required><br>
          <span id="errorName" class=""></span><br>
        </div>
        
        <div class="form-inputs">
          <label for="email" data-tr="Email"></label><br>
          <input class="input-beautiful input-focus--active" placeholder="Su email" type="${'email'}" id="${'email'}" required><br>
          <span id="errorEmail" class=""></span><br>
        </div>

        <div class="form-inputs">
          <label for="subject" data-tr="Subject"></label><br>
          <input class="input-beautiful input-focus--active" placeholder="Su duda" type="${'typeText'}" id="${'subjectText'}" pattern="[A-Za-z\sñÑ]{4,10}" required><br>
          <span id="errorSubject" class=""></span><br>
        </div>

        <div class="form-inputs">
          <label for="comment" data-tr="Comment"></label><br>
          <textarea class="input-beautiful input-focus--active" placeholder="Su comentario" name="${'commentText'}" id="${'commentText'}" cols="30" rows="10" pattern="[A-Za-z\sñÑ]{4,35}" required></textarea><br>
          <span id="errorComment" class=""></span><br>
        </div>

        <div class="form-inputs">
          <button type="button" id="submitContact" class="btn">Submit</button>
        </div>
      </form>

      <section class="contact__datosEmpresa">
        <h4 class="contact-datosEmpresa__title font-nice__title" data-tr="Company data"></h4>
        <li class="contact-datosEmpresa__name">
          <img src="../media/user.svg" alt="nombre" with="20" height="20"/>
          <label class="font-nice__content" for="name" data-tr="Name"></label><label class="font-nice__content">: ${'name'}</label>
        </li>
        <li class="contact-datosEmpresa__phone">
          <img src="../media/phone.svg" alt="mobil" with="20" height="20"/>
          <label class="font-nice__content" for="phone" data-tr="Phone"></label><label class="font-nice__content">: ${'phone'}</label>
        </li>
        <li class="contact-datosEmpresa__city">
          <img src="../media/city.svg" alt="ciudad" with="20" height="20"/>
          <label for="city" data-tr="City"></label class="font-nice__content"><label >: ${'city'}</label>
        </li>
        <li class="contact-datosEmpresa__province">
          <img src="../media/province.svg" alt="provincia" with="20" height="20"/>
          <label for="province" data-tr="Province"></label class="font-nice__content"><label>: ${'province'}</label>
        </li>
        <li class="contact-datosEmpresa__country">
          <img src="../media/country.svg" alt="pais" with="20" height="20"/>
          <label for="country" data-tr="Country"></label class="font-nice__content"><label>: ${'country'}</label>
        </li>
        <li class="contact-datosEmpresa__map">
          <h4 align="center" data-tr="Where we are?"></h4>
          <div id="map"></div>
        </li>
        
      </section>
      
    </section>
    
    `(data);
  
  return form;

}

export { templateContact };