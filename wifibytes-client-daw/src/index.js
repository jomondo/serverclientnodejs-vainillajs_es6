import {responsiveMenu,get,getCookie} from './utils/utils';
import {redirect} from './utils/redirect';
import ContactCtrl from './Components/contact/contactCtrl';
import HomeCtrl from './Components/home/homeCtrl';
import CatalogueCtrl from './Components/catalogue/catalogueCtrl';
import RatesCtrl from './Components/rates/ratesCtrl';
import {contentStatic,cookies,avisoLegal,about} from './utils/loadContent';
import {cambiarIdioma} from './lib/language';
import {Router} from './router.js';

/**
  * @function contentStatic - peticion get data,logotips,.. to head, header and footer
  */
get('/datos_empresa').then(function(response) {
  console.log("sucess contentStatic",JSON.parse(response));
  contentStatic(JSON.parse(response));
}).catch(function (error) {
  console.log("Failed!", error);
});

document.addEventListener("DOMContentLoaded",function(){
  /* when start web init home */
  Router.navigate("#");
  /* when start web init cambiarIdioma - default 'es' */
  cambiarIdioma();
  /* when the screen it have 600px, show image responsive, 
  * then if we click on it, it shows the reponsive menu
  */
  document.getElementById("icon").addEventListener("click", responsiveMenu);
  document.getElementById("btnVal").addEventListener("click", function(){
    cambiarIdioma("val");
    redirect("val");
  });
  document.getElementById("btnCast").addEventListener("click", function(){
    cambiarIdioma("es");
    redirect("es");
  });
  
});

/* routing to change controller */
Router
  .add(/contact/, function() {
    console.log("Contactesss");
    get('/datos_empresa').then(function(response) {
      console.log("res contact",response);
      new ContactCtrl("body","contact",JSON.parse(response));
    }).catch(function(error) {
      console.log("Failed contact!", error);
    });
  }).listen()
  .add(/about/, function() {
    console.log("About");
    get('/datos_empresa').then(function(response) {
      about(JSON.parse(response));
    }).catch(function(error) {
      console.log("Failed About!", error);
    });
    
  }).listen()
  .add(/rates/, function() {
    console.log("Rates");
    get('/tarifa').then(function(response) {
      console.log("tarifa",JSON.parse(response));
      new RatesCtrl("body","rates",JSON.parse(response));
    }).catch(function(error) {
      console.log("Failed About!", error);
    });
    
  }).listen()
  .add(/catalogue/, function() {
    console.log("Catalogo");
    get('/articulo').then(function(response) {
      let catalogue = JSON.parse(response);
      console.log("cat",catalogue.results);
      get('/familia').then(function(response) {
        let familia = JSON.parse(response);
        console.log("familia",familia);
        new CatalogueCtrl("body","catalogue",catalogue.results,familia);
      }).catch(function(error) {
        console.log("Failed About!", error);
      });
      
    }).catch(function(error) {
      console.log("Failed About!", error);
    });
    
  }).listen().add(/aviso-legal/, function() {
    console.log("aviso-legal");
    get('/datos_empresa').then(function(response) {
      avisoLegal(JSON.parse(response));
    }).catch(function(error) {
      console.log("Failed aviso-legal!", error);
    });
    
  }).listen().add(/cookies/, function() {
    console.log("cookies");
    get('/datos_empresa').then(function(response) {
      cookies(JSON.parse(response));
    }).catch(function(error) {
      console.log("Failed cookies!", error);
    });
    
  }).listen().add(/home/, function() {
    get('/home').then(function(responseHome) {
      responseHome = JSON.parse(responseHome);
      console.log("ssssssHome",responseHome);
      let contentHome = responseHome.filter(home =>home.lang==getCookie("lang"));
      get('/tarifa/').then(function(responseTarifas) {
        console.log("tarifas",JSON.parse(responseTarifas));
        new HomeCtrl("body","home",contentHome,JSON.parse(responseTarifas));
      }).catch(function(error) {
        console.log("Failed tarifa!", error);
      });
      
    }).catch(function(error) {
      console.log("Failed home!", error);
    });
    
  })
  .add(function() {
    console.log('default');
    get('/home').then(function(responseHome) {
      responseHome = JSON.parse(responseHome);
      console.log("defaultssHome",responseHome);
      let contentHome = responseHome.filter(home =>home.lang==getCookie("lang"));
      get('/tarifa/').then(function(responseTarifas) {
        console.log("tarifas",JSON.parse(responseTarifas));
        new HomeCtrl("body","home",contentHome,JSON.parse(responseTarifas));
      }).catch(function(error) {
        console.log("Failed tarifa!", error);
      });
      
    }).catch(function(error) {
      console.log("Failed home!", error);
    });
      
  });



    