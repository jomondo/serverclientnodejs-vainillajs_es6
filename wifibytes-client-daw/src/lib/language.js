import { setCookie, getCookie } from '../utils/utils';

/**
 * @param {String} lang - language preference
* @function cambiarIdioma - Change the content according to the language and load a json 
* with the preferred language of choice. Detect data-tr in the html and change it.
*/

function cambiarIdioma(lang) {
  //save preference on cookies or default 'es'
  lang = lang || getCookie("lang") || 'es';
  setCookie("lang", lang);
  var frases = require("./i18n/" + lang + ".json");
  console.log("ha pillat frases", frases);

  var elems = document.querySelectorAll('[data-tr]');
  elems.forEach(function (element, index, array) {
    array[index].innerHTML = frases
      ? frases[array[index].dataset.tr]
      : array[index].dataset.tr;
  });
}

export { cambiarIdioma };