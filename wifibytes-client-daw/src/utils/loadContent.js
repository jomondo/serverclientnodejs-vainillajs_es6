import { head, header, footer } from '../layout/layout';
import {
    templateCookies,
    templateAvisoLegal,
    templateAbout
} from '../templates';
import  CarouselCtrl  from '../Components/carousel/carouselCtrl';
/**
* @module utils - utils all apps
*/

/**
* @function contentStatic - show static content as head,header,carousel,footer
*/
function contentStatic(response) {
    head(response);
    header(response);
    new CarouselCtrl("carousel","carousel",response);
    footer(response);
}
/**
* @function cookies - show content cookies
*/
function cookies(response) {
    let content = document.getElementById("body");
    let cookies = response.textos.filter(text => text.key == "cookies");
    cookies.map((element) => {
        let data = { id: element.key, content: element.content };
        let contentCookies = templateCookies(data);
        content.innerHTML = contentCookies;
    });

}
/**
* @function avisoLegal - show content aviso legal
*/

function avisoLegal(response) {
    let content = document.getElementById("body");
    let avisoLegal = response.textos.filter(text => text.key == "aviso-legal");
    avisoLegal.map((element) => {
        let data = { id: element.key, content: element.content };
        let contentAvisoLegal = templateAvisoLegal(data);
        content.innerHTML = contentAvisoLegal;
    });
}
/**
  * @function about - show content about
  */
function about(response) {
    let content = document.getElementById("body");
    let about = response.textos.filter(text => text.key == "sobre-nosotros");
    about.map((element) => {
        let data = { id: element.key, content: element.content };
        let contentAbout = templateAbout(data);
        content.innerHTML = contentAbout;
    });

}

export { contentStatic, cookies, avisoLegal, about };