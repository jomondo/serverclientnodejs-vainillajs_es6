// From Jake Archibald's Promises and Back:
// http://www.html5rocks.com/en/tutorials/es6/promises/#toc-promisifying-xmlhttprequest
import {Settings} from '../settings';

let CACHE_TEMPLATES = new Map();

/**
 * @param {String} url - url destination
* @function get - peticion to get data where return a new promise
*/
function get(url) {
  /* every 5 minutes we clean the cache */
  if(!localStorage.getItem("times")){
    localStorage.setItem("times",new Date().getTime() + 300000);//5 min
  }
  if(localStorage.getItem("times") <= new Date().getTime()){
    console.log("se t'ha passat el temps xato");
    localStorage.removeItem("times");
    CACHE_TEMPLATES.clear();
  }
  
  return new Promise(function(resolve, reject) {
    //console.log("cache",CACHE_TEMPLATES);
    if (CACHE_TEMPLATES.has(url)) {
      resolve(CACHE_TEMPLATES.get(url));
    }else{
      // Do the usual XHR stuff
      var req = new XMLHttpRequest();
      req.open('GET', Settings.baseURL+url);
    
      req.onload = function() {
        // This is called even on 404 etc
        // so check the status
        if (req.status == 200) {
          // Resolve the promise with the response text
          CACHE_TEMPLATES.set(Settings.baseURL+url,req.response);
          resolve(req.response);
        }
        else {
          // Otherwise reject with the status text
          // which will hopefully be a meaningful error
          reject(Error(req.statusText));
        }
      };
    
      // Handle network errors
      req.onerror = function() {
        reject(Error("Network Error"));
      };
    
      // Make the request
      req.send();
    }
      
  });
}
/**
* @param {String} url - url post destination
* @param {String:Object} data - can be data as string or as objects
* @function postAjax - peticion to send data on server
*/
function postAjax(url, data, success) {
  var params = typeof data == 'string' ? data : Object.keys(data).map(
    function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
  ).join('&');
  
  var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
  xhr.open('POST', url);
  xhr.onreadystatechange = function() {
  if (xhr.readyState>3 && xhr.status==200) { console.log(xhr.responseText);
    success(xhr.responseText); }
  };
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.send(params);
  console.log(xhr);
  return xhr;
}

/**
* @param {String} strings - elements html
* @param {Object} keys - keys that are injected into the template
* @function generaTemplate - It goes through the object of the keys and, 
* for each one of them that coincide with the key, it is injected.
* @returns {String}
*/
function generaTemplate (strings, ...keys) {
  return function(data) {
    let temp = strings.slice();
    keys.forEach((key, i) => {
      temp[i] = temp[i] + data[key];
    });
    return temp.join('');
  }
}
/**
* @function responsiveMenu - Toggle between adding and removing the "responsive" 
* class to topnav when the user clicks on the icon
*/
function responsiveMenu() {
  var x = document.getElementById("myTopnav");
  if (x.className === "top-nav") {
      x.className += " responsive";
  } else {
      x.className = "top-nav";
  }
}
/**
* @function toaster - show information for user (success,info,warning,errors)
*/
function toaster(element,type,message){
  element.className = "toaster toaster-"+type;
  element.innerHTML = message;
}
/**
* @function setCookie - set a cookie through a parameter
*/
function setCookie(key, value) {
  var expires = new Date();
  expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
  document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
}
/**
* @function getCookie - get a cookie through a parameter
*/
function getCookie(key) {
  var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
  return keyValue ? keyValue[2] : null;
}

export {get, responsiveMenu, generaTemplate, postAjax, toaster, setCookie, getCookie};