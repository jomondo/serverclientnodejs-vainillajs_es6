import { Router } from '../router.js';

/**
* @function redirect - show where we are and redirect to load the language
* @param {String} rute - idiom preference
*/
function redirect(rute) {
    let page = Router.getFragment();
    console.log("sV", page);
    switch (rute) {
        case "es":
            if (page == "#" || page == "" || page == "home/lang=val") Router.navigate("/home/lang=es");
            break;
        case "val":
            if (page == "#" || page == "" || page == "home/lang=es") Router.navigate("/home/lang=val");
            break;
    }
}

export { redirect };