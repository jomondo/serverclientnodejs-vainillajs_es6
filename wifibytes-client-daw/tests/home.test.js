import HomeCtrl from '../src/Components/home/homeCtrl';
import * as homeCtrl from '../src/Components/home/homeCtrl';
import {dataHome,dataTarifas} from "./fakeData/home";
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
    document.body.innerHTML =`
    <section id="body">
      <h1 class="home-title"></h1>
      <h3 class="home-subtitle"></h3>
      
      <section class="home-text">
        <div class="home-leftBox">
            <h3 class="home-leftBox__title"></h3>
        </div>
        
      </section>
      <aside>
        <div class="home-rightBox">
          <h3 class="home-rightBox__title"></h3>
        </div>
      </aside>
      
    </section>
  `;

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');

  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

fakeDOMLoaded();

});
test('This test check if HomeCtrl called the class constructor', () => {
  let home = new HomeCtrl("body","home",dataHome,dataTarifas);
  expect(home.constructor.name).toBe("HomeCtrl");
});

test('This test check that HomeCtrl class is called', () => {
  new HomeCtrl("body","home",dataHome,dataTarifas);
  let homeTitle = $("h1.home-title").html();
  expect(homeTitle).toBe(dataHome[0].titulo);

  let tarifas = $(".home-tarifas__title").html();
  expect(tarifas).toBe(dataTarifas.results[0].nombretarifa);
});


