import {getTemplate} from '../src/templates/index';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =`
  <section id="body">
    
  </section>
  `;

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');

  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

fakeDOMLoaded();

});

test('Check getTemplate catalogue', () => {
    getTemplate("body","catalogue");
    expect($("#body").children.length).toBeGreaterThan(1);
});

test('Check getTemplate error', () => {
    let a = getTemplate("bod","catalogue");
    expect(a).toBe(undefined);
});

test('Check getTemplate rates ', () => {
    getTemplate("body","rates");
    expect($("#body").children.length).toBeGreaterThan(1);
});