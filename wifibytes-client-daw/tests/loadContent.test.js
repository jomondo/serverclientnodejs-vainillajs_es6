import {contentHome,cookies,avisoLegal,about} from '../src/utils/loadContent';
import {dataTarifas,dataHome} from './fakeData/home';
import {dataCookies} from './fakeData/cookies';
import {dataAvisoLegal} from './fakeData/aviso-legal';
import {dataAbout} from './fakeData/about';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =`
  <section id="body"></section>
  `;

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');

  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

fakeDOMLoaded();

});

test('Check cookies', () => {
    cookies(dataCookies);
    let cookiesHTML = $("#body >*").length;
    console.log("cookies",cookiesHTML);
    expect(cookiesHTML).toBe(1);
});

test('Check avisoLegal', () => {
    avisoLegal(dataAvisoLegal);
    let avisoHTML = $("#body >*").length;
    console.log("avisoLegal",avisoHTML);
    expect(avisoHTML).toBe(1);
});

test('Check about', () => {
    about(dataAbout);
    let aboutHTML = $("#body >*").length;
    console.log("about",aboutHTML);
    expect(aboutHTML).toBe(1);
});
