import {Router} from '../src/router';
//import {Router} from '../src/router';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
    document.body.innerHTML =`
    <nav class="top-nav" id="myTopnav">
      <a href="" class="top-nav__itemLogo"><img src="" id="header_logo" alt=""></a>
      <a href="#" id="home" class="top-nav__item top-nav__item--active" data-tr="Inicio">Home</a>
      <a href="#contact" id="contact" class="top-nav__item" data-tr="Contacto">Contacto</a>
      <a href="#about" id="contact" class="top-nav__item" data-tr="Contacto">About</a>
      <a href="javascript:void(0);" class="top-nav__item" id="icon">
        <i class=""><img src="./media/responsive.svg" alt="responsive" height="16" width="16"></i>
      </a>
      <input type="submit" value="Castellano" class="btn btn__info" id="btnCast">
      <input type="submit" value="Valencia" class="btn btn__info" id="btnVal">
    </nav>
  `;

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');

  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

fakeDOMLoaded();

});

test('Check router add', () => {
  let route = "/contactos/";
  var routeAdd = Router.add(route);
  expect(routeAdd.routes[0].re).toBe(route);
});

test('Check router config', () => {
  let modes = "history";
  let conf = Router.config({ mode: modes});
  expect(conf.mode).toBe(modes);
});

test('Check router remove', () => {
  let conf = Router.remove("/contactos/");
  console.log("remove",conf.routes.length);
  expect(conf.routes.length).toBe(0);
});

test('Check where we are at the moment (getFragment,clearSlashes,check,listen and navigate router)', () => {
  let route = "/contact/";
  Router.navigate("/contact/");
  Router.add(route).listen();
  let fragment = Router.getFragment();
  //console.log("h",Router.getFragment());
  expect(fragment).toBe("contact");
});

