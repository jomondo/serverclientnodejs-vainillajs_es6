import  ContactCtrl from '../src/Components/contact/contactCtrl';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
    document.body.innerHTML =`
    <section class="contact" id="body">
      
    </section>
  `;

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');

  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

fakeDOMLoaded();

});
test('This test check if Contact called the class constructor', () => {
  let data = {
    name: "SiempreOnline",
    phone: 602545872,
    city: "Ontinyent",
    province: "Valencia",
    country: "Espanya",
    idForm: "form-contact"
  };
  const contact = new ContactCtrl("body","contact",data); 
  expect(contact.constructor.name).toBe('ContactCtrl');
});

test('This test check that contact function is called', () => {
  let data = {
    name: "SiempreOnline",
    phone: 602545872,
    city: "Ontinyent",
    province: "Valencia",
    country: "Espanya",
    idForm: "form-contact"
  };
  new ContactCtrl("body","contact",data);
  let contactHTML = $(".contact > *").children.length;
  expect(contactHTML).toBeGreaterThan(1); 
});



