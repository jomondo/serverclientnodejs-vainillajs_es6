import CarouselCtrl from '../src/Components/carousel/carouselCtrl';
import {data} from './fakeData/carousel';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
    document.body.innerHTML =`
    <section id="carousel" class="carousel">
        <div id="jumbotron" class="jumbotrons"></div>
        <div id="indicators" class="indicators">
          <div id="jumbotron-text" class="jumbotron-text"></div>
          <input class="indicator" name="indicator" data-slide="1" data-state="active" checked type="radio" />
          <input class="indicator" name="indicator" data-slide="2" type="radio" />
          <input class="indicator" name="indicator" data-slide="3" type="radio" />
        </div>
    </section>
  `;

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');

  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

fakeDOMLoaded();

});
test('This test check if CarouselCtrl called the class constructor', () => {
    
    const carousel = new CarouselCtrl("carousel","carousel",data);
    expect(carousel.constructor.name).toBe('CarouselCtrl');   
});
test('This test check that 3 jumbotron are inyected inside to id=jumbotron', () => {
    new CarouselCtrl("carousel","carousel",data);
    
    let jumboLength = $(".jumbotron").length;
    expect(jumboLength).toBe(3); 

    let jumbo = $(".jumbotron").attr("src");
    expect(jumbo).toBe(data.textos[0].image);   
});

