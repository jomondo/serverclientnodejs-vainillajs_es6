const dataTarifas =
{
  results: [
    {
      "codtarifa": 1,
      "nombretarifa": "Plan Básico",
      "slug": "plan-basico",
      "pretitulo": "básico",
      "logo": "http://localhost:8000/media/Logo/coins.svg",
      "precio": 10,
      "activo": true,
      "destacado": true,
      "color": {
        "id": 1,
        "titulo": "black",
        "hexadecimal": "#434040"
      },
      "subtarifas": [
        {
          "subtarifa_id": 1,
          "subtarifa_datos_internet": 4,
          "subtarifa_cent_minuto": 1,
          "subtarifa_est_llamada": 1,
          "subtarifa_precio_sms": 2,
          "subtarifa_minutos_gratis": 200,
          "subtarifa_minutos_ilimitados": false,
          "subtarifa_velocidad_conexion_subida": 9,
          "subtarifa_velocidad_conexion_bajada": 3,
          "subtarifa_num_canales": 5,
          "subtarifa_siglas_omv": "TEST",
          "subtarifa_omv": null,
          "tipo_tarifa": 1,
          "subtarifa_tarifa": {
            "codtarifa": 1,
            "nombretarifa": "Plan Básico",
            "slug": "plan-basico",
            "pretitulo": "básico",
            "pretitulo_va": "test",
            "logo": "media/Logo/coins.svg",
            "precio": 10,
            "activo": true,
            "destacado": true,
            "created_at": 1540928832,
            "updated_at": 1541590654,
            "color": 1
          }
        }
      ]
    }
  ]
};
const dataHome = [{
  activo: true,
  caja_derecha_texto: "<p>Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right Lorem ipsum right</p>",
  caja_derecha_titulo: "TitleRightBox",
  caja_izquierda_texto: "<p>Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left Lorem ipsum left</p>",
  caja_izquierda_titulo: "TitleLeftBox",
  idioma: 1,
  lang: "CastellanoC1",
  pk: 1,
  subtitulo: "subtitleHome",
  titulo: "TitleHome"
}];
export { dataTarifas, dataHome };