var rates = {
  "count": 4,
  "next": null,
  "previous": null,
  "results": [
    {
      "codtarifa": 1,
      "nombretarifa": "Plan Básico",
      "slug": "plan-basico",
      "pretitulo": "básico",
      "logo": "http://localhost:8000/media/Logo/coins.svg",
      "precio": 10,
      "activo": true,
      "destacado": true,
      "color": {
        "id": 1,
        "titulo": "black",
        "hexadecimal": "#434040"
      },
      "subtarifas": [
        {
          "subtarifa_id": 1,
          "subtarifa_datos_internet": 4,
          "subtarifa_cent_minuto": 1,
          "subtarifa_est_llamada": 1,
          "subtarifa_precio_sms": 2,
          "subtarifa_minutos_gratis": 200,
          "subtarifa_minutos_ilimitados": false,
          "subtarifa_velocidad_conexion_subida": 9,
          "subtarifa_velocidad_conexion_bajada": 3,
          "subtarifa_num_canales": 5,
          "subtarifa_siglas_omv": "TEST",
          "subtarifa_omv": null,
          "tipo_tarifa": 1,
          "subtarifa_tarifa": {
            "codtarifa": 1,
            "nombretarifa": "Plan Básico",
            "slug": "plan-basico",
            "pretitulo": "básico",
            "pretitulo_va": "test",
            "logo": "media/Logo/coins.svg",
            "precio": 10,
            "activo": true,
            "destacado": true,
            "created_at": 1540928832,
            "updated_at": 1541590654,
            "color": 1
          }
        }
      ]
    },
    {
      "codtarifa": 2,
      "nombretarifa": "Plan Intermedio",
      "slug": "plan-intermedio",
      "pretitulo": "Intermedio",
      "logo": "http://localhost:8000/media/Logo/customer-service.svg",
      "precio": 20,
      "activo": true,
      "destacado": true,
      "color": {
        "id": 1,
        "titulo": "black",
        "hexadecimal": "#434040"
      },
      "subtarifas": [
        {
          "subtarifa_id": 2,
          "subtarifa_datos_internet": 2,
          "subtarifa_cent_minuto": 2,
          "subtarifa_est_llamada": 2,
          "subtarifa_precio_sms": 2,
          "subtarifa_minutos_gratis": 77,
          "subtarifa_minutos_ilimitados": false,
          "subtarifa_velocidad_conexion_subida": 2,
          "subtarifa_velocidad_conexion_bajada": 2,
          "subtarifa_num_canales": 5,
          "subtarifa_siglas_omv": "3",
          "subtarifa_omv": null,
          "tipo_tarifa": 1,
          "subtarifa_tarifa": {
            "codtarifa": 2,
            "nombretarifa": "Plan Intermedio",
            "slug": "plan-intermedio",
            "pretitulo": "Intermedio",
            "pretitulo_va": "",
            "logo": "media/Logo/customer-service.svg",
            "precio": 20,
            "activo": true,
            "destacado": true,
            "created_at": 1540932470,
            "updated_at": 1541505576,
            "color": 1
          }
        }
      ]
    },
    {
      "codtarifa": 3,
      "nombretarifa": "Plan Experto",
      "slug": "plan-experto",
      "pretitulo": "Experto",
      "logo": "http://localhost:8000/media/Logo/detective.svg",
      "precio": 35,
      "activo": true,
      "destacado": true,
      "color": {
        "id": 1,
        "titulo": "black",
        "hexadecimal": "#434040"
      },
      "subtarifas": []
    },
    {
      "codtarifa": 4,
      "nombretarifa": "Plan User",
      "slug": "plan-user",
      "pretitulo": "User",
      "logo": "http://localhost:8000/media/Logo/solidarity-purchase2.svg",
      "precio": 22,
      "activo": true,
      "destacado": false,
      "color": {
        "id": 1,
        "titulo": "black",
        "hexadecimal": "#434040"
      },
      "subtarifas": []
    }
  ]
};

  export{rates};