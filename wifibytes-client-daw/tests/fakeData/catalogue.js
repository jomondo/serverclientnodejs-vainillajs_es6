var catalogue = [
    {
      "referencia": "cd24c6a5-ed63-430f-8e6c-09b491a71c86",
      "templates": {
        "template3": {
          "pretitulo": "",
          "franja_1_texto": "",
          "franja_1_fondo": null,
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "franja_2_texto": "",
          "franja_2_fondo": null,
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen4": null,
          "articulo": null,
          "idioma": null
        },
        "template2": {
          "pretitulo": "",
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "caja_3_titulo": "",
          "caja_3_texto": "",
          "caja_4_titulo": "",
          "caja_4_texto": "",
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen4": null,
          "imagen_fondo_cabecera": null,
          "imagen_fondo_cuerpo": null,
          "articulo": null,
          "idioma": null
        },
        "template1": {
          "pretitulo": "",
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "caja_3_titulo": "",
          "caja_3_texto": "",
          "caja_4_titulo": "",
          "caja_4_texto": "",
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen_fondo_cabecera": null,
          "imagen_fondo_cuerpo": null,
          "articulo": null,
          "idioma": null
        }
      },
      "descripcion": "Smartphone BQ Android 10\"",
      "descripcion_breve": "Description breve spanish",
      "descripcion_larga": "<p>Especificaciones spanish Especificaciones spanish Especificaciones spanish Especificaciones spanish Especificaciones spanish Especificaciones spanish Especificaciones spanish</p>",
      "imagen": "http://localhost:8000/media/pagina_tarifas/bq.jpg",
      "thumbnail": "http://localhost:8000/media/pagina_tarifas/bq_yQm7wDP.jpg",
      "slug": "smartphone-bq-android-10",
      "descripcion_va": "Smartphone BQ Android 10\"",
      "pvp": 50,
      "stockfis": 4,
      "descripcion_breve_va": "Description breve valencia ",
      "descripcion_larga_va": "<p>Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia</p>",
      "template": 1,
      "activo": true,
      "visible": true,
      "destacado": false,
      "secompra": true,
      "stockmax": 0,
      "codimpuesto": "IVA21%",
      "observaciones": "",
      "codbarras": "",
      "nostock": false,
      "controlstock": false,
      "tipocodbarras": null,
      "sevende": true,
      "venta_online": true,
      "stockmin": 0,
      "created_at": 1542362332,
      "updated_at": 1542484508,
      "codfamilia": "#S1",
      "marca": 1,
      "pantalla": 1,
      "procesador": 1,
      "ram": 1,
      "camara": 1
    },
    {
      "referencia": "267c6af5-1a19-482a-a4c1-40c922da94b7",
      "templates": {
        "template3": {
          "pretitulo": "",
          "franja_1_texto": "",
          "franja_1_fondo": null,
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "franja_2_texto": "",
          "franja_2_fondo": null,
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen4": null,
          "articulo": null,
          "idioma": null
        },
        "template2": {
          "pretitulo": "",
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "caja_3_titulo": "",
          "caja_3_texto": "",
          "caja_4_titulo": "",
          "caja_4_texto": "",
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen4": null,
          "imagen_fondo_cabecera": null,
          "imagen_fondo_cuerpo": null,
          "articulo": null,
          "idioma": null
        },
        "template1": {
          "pretitulo": "",
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "caja_3_titulo": "",
          "caja_3_texto": "",
          "caja_4_titulo": "",
          "caja_4_texto": "",
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen_fondo_cabecera": null,
          "imagen_fondo_cuerpo": null,
          "articulo": null,
          "idioma": null
        }
      },
      "descripcion": "Smartphone Samsung Android 12\"",
      "descripcion_breve": "Description breve spanish",
      "descripcion_larga": "<p>Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol</p>",
      "imagen": "http://localhost:8000/media/pagina_tarifas/samsung.png",
      "thumbnail": "http://localhost:8000/media/pagina_tarifas/samsung_tPmN2M9.png",
      "slug": "smartphone-samsung-android-12",
      "descripcion_va": "Description valencia Description valencia",
      "pvp": 285,
      "stockfis": 2,
      "descripcion_breve_va": "Description breve valencia",
      "descripcion_larga_va": "<p>Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia</p>",
      "template": 1,
      "activo": true,
      "visible": true,
      "destacado": false,
      "secompra": true,
      "stockmax": 0,
      "codimpuesto": "IVA21%",
      "observaciones": "",
      "codbarras": "",
      "nostock": false,
      "controlstock": false,
      "tipocodbarras": null,
      "sevende": true,
      "venta_online": true,
      "stockmin": 0,
      "created_at": 1542362557,
      "updated_at": 1542485995,
      "codfamilia": "#S1",
      "marca": 2,
      "pantalla": 2,
      "procesador": 1,
      "ram": 2,
      "camara": 1
    },
    {
      "referencia": "7d57d613-8e44-40d3-a473-49ac54e3ba1b",
      "templates": {
        "template3": {
          "pretitulo": "",
          "franja_1_texto": "",
          "franja_1_fondo": null,
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "franja_2_texto": "",
          "franja_2_fondo": null,
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen4": null,
          "articulo": null,
          "idioma": null
        },
        "template2": {
          "pretitulo": "",
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "caja_3_titulo": "",
          "caja_3_texto": "",
          "caja_4_titulo": "",
          "caja_4_texto": "",
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen4": null,
          "imagen_fondo_cabecera": null,
          "imagen_fondo_cuerpo": null,
          "articulo": null,
          "idioma": null
        },
        "template1": {
          "pretitulo": "",
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "caja_3_titulo": "",
          "caja_3_texto": "",
          "caja_4_titulo": "",
          "caja_4_texto": "",
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen_fondo_cabecera": null,
          "imagen_fondo_cuerpo": null,
          "articulo": null,
          "idioma": null
        }
      },
      "descripcion": "Smartphone Xiaomi Android 12\"",
      "descripcion_breve": "Description breve spanish",
      "descripcion_larga": "<p>Espedificaciones espa&ntilde;ol Espedificaciones espa&ntilde;ol Espedificaciones espa&ntilde;ol Espedificaciones espa&ntilde;ol Espedificaciones espa&ntilde;ol Espedificaciones espa&ntilde;ol</p>",
      "imagen": "http://localhost:8000/media/pagina_tarifas/xiaomi.jpg",
      "thumbnail": "http://localhost:8000/media/pagina_tarifas/xiaomi_i2G15fm.jpg",
      "slug": "smartphone-xiaomi-android-12",
      "descripcion_va": "Smartphone Xiaomi Android 12\"",
      "pvp": 150,
      "stockfis": 1,
      "descripcion_breve_va": "Description breve valencia",
      "descripcion_larga_va": "<p>Espedificaciones valencia Espedificaciones valencia Espedificaciones valencia Espedificaciones valencia Espedificaciones valencia Espedificaciones valencia Espedificaciones valencia</p>",
      "template": 1,
      "activo": true,
      "visible": true,
      "destacado": false,
      "secompra": true,
      "stockmax": 0,
      "codimpuesto": "IVA21%",
      "observaciones": "",
      "codbarras": "",
      "nostock": false,
      "controlstock": false,
      "tipocodbarras": null,
      "sevende": true,
      "venta_online": true,
      "stockmin": 0,
      "created_at": 1542486920,
      "updated_at": 1542486920,
      "codfamilia": "#S1",
      "marca": 3,
      "pantalla": 1,
      "procesador": 1,
      "ram": 1,
      "camara": 1
    },
    {
      "referencia": "2e91070e-0e63-4cd2-9550-b32038ceac5b",
      "templates": {
        "template3": {
          "pretitulo": "",
          "franja_1_texto": "",
          "franja_1_fondo": null,
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "franja_2_texto": "",
          "franja_2_fondo": null,
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen4": null,
          "articulo": null,
          "idioma": null
        },
        "template2": {
          "pretitulo": "",
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "caja_3_titulo": "",
          "caja_3_texto": "",
          "caja_4_titulo": "",
          "caja_4_texto": "",
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen4": null,
          "imagen_fondo_cabecera": null,
          "imagen_fondo_cuerpo": null,
          "articulo": null,
          "idioma": null
        },
        "template1": {
          "pretitulo": "",
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "caja_3_titulo": "",
          "caja_3_texto": "",
          "caja_4_titulo": "",
          "caja_4_texto": "",
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen_fondo_cabecera": null,
          "imagen_fondo_cuerpo": null,
          "articulo": null,
          "idioma": null
        }
      },
      "descripcion": "Smartphone Xiaomi Readmi 11\"",
      "descripcion_breve": "Description breve spanish",
      "descripcion_larga": "<p>Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol</p>",
      "imagen": "http://localhost:8000/media/pagina_tarifas/xiaomi-redmi.jpg",
      "thumbnail": "http://localhost:8000/media/pagina_tarifas/xiaomi-redmi_TbmY9jS.jpg",
      "slug": "smartphone-xiaomi-readmi-11",
      "descripcion_va": "Smartphone Xiaomi Readmi 11\"",
      "pvp": 0,
      "stockfis": 0,
      "descripcion_breve_va": "Description breve valencia",
      "descripcion_larga_va": "<p>Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia</p>",
      "template": 1,
      "activo": true,
      "visible": true,
      "destacado": false,
      "secompra": true,
      "stockmax": 0,
      "codimpuesto": "IVA21%",
      "observaciones": "",
      "codbarras": "",
      "nostock": false,
      "controlstock": false,
      "tipocodbarras": null,
      "sevende": true,
      "venta_online": true,
      "stockmin": 0,
      "created_at": 1542489349,
      "updated_at": 1542489370,
      "codfamilia": "#S1",
      "marca": 3,
      "pantalla": 2,
      "procesador": 1,
      "ram": 2,
      "camara": 1
    },
    {
      "referencia": "4d932523-242b-4c99-a7a2-64560dde41c1",
      "templates": {
        "template3": {
          "pretitulo": "",
          "franja_1_texto": "",
          "franja_1_fondo": null,
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "franja_2_texto": "",
          "franja_2_fondo": null,
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen4": null,
          "articulo": null,
          "idioma": null
        },
        "template2": {
          "pretitulo": "",
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "caja_3_titulo": "",
          "caja_3_texto": "",
          "caja_4_titulo": "",
          "caja_4_texto": "",
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen4": null,
          "imagen_fondo_cabecera": null,
          "imagen_fondo_cuerpo": null,
          "articulo": null,
          "idioma": null
        },
        "template1": {
          "pretitulo": "",
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "caja_3_titulo": "",
          "caja_3_texto": "",
          "caja_4_titulo": "",
          "caja_4_texto": "",
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen_fondo_cabecera": null,
          "imagen_fondo_cuerpo": null,
          "articulo": null,
          "idioma": null
        }
      },
      "descripcion": "Smartphone iphone 6 7\" IOS",
      "descripcion_breve": "Description breve spanish",
      "descripcion_larga": "<p>Espedificaciones espa&ntilde;ol Espedificaciones espa&ntilde;ol Espedificaciones espa&ntilde;ol Espedificaciones espa&ntilde;ol Espedificaciones espa&ntilde;ol Espedificaciones espa&ntilde;ol Espedificaciones espa&ntilde;ol</p>",
      "imagen": "http://localhost:8000/media/pagina_tarifas/iphone.jpg",
      "thumbnail": "http://localhost:8000/media/pagina_tarifas/iphone_xodKiXX.jpg",
      "slug": "smartphone-iphone-6-7-ios",
      "descripcion_va": "Smartphone iphone 6 7\" IOS",
      "pvp": 180,
      "stockfis": 0,
      "descripcion_breve_va": "Description breve valencia",
      "descripcion_larga_va": "<p>Espedificaciones valencia Espedificaciones valencia Espedificaciones valencia Espedificaciones valencia Espedificaciones valencia Espedificaciones valencia Espedificaciones valencia</p>",
      "template": 1,
      "activo": true,
      "visible": true,
      "destacado": false,
      "secompra": true,
      "stockmax": 0,
      "codimpuesto": "IVA21%",
      "observaciones": "",
      "codbarras": "",
      "nostock": false,
      "controlstock": false,
      "tipocodbarras": null,
      "sevende": true,
      "venta_online": true,
      "stockmin": 0,
      "created_at": 1542489600,
      "updated_at": 1542489600,
      "codfamilia": "#S1",
      "marca": 4,
      "pantalla": 1,
      "procesador": 1,
      "ram": 2,
      "camara": 1
    },
    {
      "referencia": "1ec2c446-69f2-454e-adcd-b061cc2e0b84",
      "templates": {
        "template3": {
          "pretitulo": "",
          "franja_1_texto": "",
          "franja_1_fondo": null,
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "franja_2_texto": "",
          "franja_2_fondo": null,
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen4": null,
          "articulo": null,
          "idioma": null
        },
        "template2": {
          "pretitulo": "",
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "caja_3_titulo": "",
          "caja_3_texto": "",
          "caja_4_titulo": "",
          "caja_4_texto": "",
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen4": null,
          "imagen_fondo_cabecera": null,
          "imagen_fondo_cuerpo": null,
          "articulo": null,
          "idioma": null
        },
        "template1": {
          "pretitulo": "",
          "caja_1_titulo": "",
          "caja_1_texto": "",
          "caja_2_titulo": "",
          "caja_2_texto": "",
          "caja_3_titulo": "",
          "caja_3_texto": "",
          "caja_4_titulo": "",
          "caja_4_texto": "",
          "imagen1": null,
          "imagen2": null,
          "imagen3": null,
          "imagen_fondo_cabecera": null,
          "imagen_fondo_cuerpo": null,
          "articulo": null,
          "idioma": null
        }
      },
      "descripcion": "Tablet Asus 10\"",
      "descripcion_breve": "Description breve spanish",
      "descripcion_larga": "<p>Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol Especificaciones espa&ntilde;ol</p>",
      "imagen": "http://localhost:8000/media/pagina_tarifas/tablets.png",
      "thumbnail": "http://localhost:8000/media/pagina_tarifas/tablets_vl0il4b.png",
      "slug": "tablet-asus-10",
      "descripcion_va": "Tablet Asus 10\"",
      "pvp": 100,
      "stockfis": 0,
      "descripcion_breve_va": "Description breve valencia",
      "descripcion_larga_va": "<p>Especificaciones valencia&nbsp; Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia Especificaciones valencia </p>",
      "template": 1,
      "activo": true,
      "visible": true,
      "destacado": false,
      "secompra": true,
      "stockmax": 0,
      "codimpuesto": "IVA21%",
      "observaciones": "",
      "codbarras": "",
      "nostock": false,
      "controlstock": false,
      "tipocodbarras": null,
      "sevende": true,
      "venta_online": true,
      "stockmin": 0,
      "created_at": 1542539136,
      "updated_at": 1542539136,
      "codfamilia": "#T1",
      "marca": 5,
      "pantalla": 1,
      "procesador": 1,
      "ram": 2,
      "camara": 1
    }
  ];

  var familia = [{
    "count": 2,
    "next": null,
    "previous": null,
    results: [
      {
        "codfamilia": "#S1",
        "slug": "smartphones",
        "nombre": "Smartphones",
        "color": {
          "id": 1,
          "titulo": "black",
          "hexadecimal": "#434040"
        },
        "icono": "http://localhost:8000/media/familia/smartphone.svg",
        "pretitulo": "Smartphones",
        "titulo": "Smartphones",
        "precio_cabecera": 100,
        "imagen_cabecera": "http://localhost:8000/media/familia/smartphone_ltYeAp3.svg",
        "thumbnail": null,
        "texto_cabecera": "<p>Lorem ipsum spanish Lorem ipsum spanish Lorem ipsum spanish Lorem ipsum spanish Lorem ipsum spanish Lorem ipsum spanish Lorem ipsum spanish Lorem ipsum spanish</p>",
        "subtexto_cabecera": "<p>Lorem subtext spanish Lorem subtext spanish Lorem subtext spanish Lorem subtext spanish Lorem subtext spanish Lorem subtext spanish Lorem subtext spanish Lorem subtext spanish Lorem subtext spanish Lorem subtext spanish</p>"
      },
      {
        "codfamilia": "#T1",
        "slug": "tablets",
        "nombre": "Tablets",
        "color": {
          "id": 1,
          "titulo": "black",
          "hexadecimal": "#434040"
        },
        "icono": "http://localhost:8000/media/familia/tablets.png",
        "pretitulo": "Tablets",
        "titulo": "Variedad de tablets",
        "precio_cabecera": 7,
        "imagen_cabecera": "http://localhost:8000/media/familia/tablets_Gx6sSVk.png",
        "thumbnail": null,
        "texto_cabecera": "<p>Description espa&ntilde;ol Description espa&ntilde;ol Description espa&ntilde;ol Description espa&ntilde;ol Description espa&ntilde;ol Description espa&ntilde;ol Description espa&ntilde;ol Description espa&ntilde;ol</p>",
        "subtexto_cabecera": "<p>Description espa&ntilde;ol Description espa&ntilde;ol Description espa&ntilde;ol Description espa&ntilde;ol Description espa&ntilde;ol Description espa&ntilde;ol Description espa&ntilde;ol Description espa&ntilde;ol</p>"
      }
    ]
  }];

  export {catalogue, familia};