import RatesCtrl from '../src/Components/rates/ratesCtrl';
import {rates} from "./fakeData/rates";
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
    document.body.innerHTML =`
    <section id="body">
    
    </section>
  `;

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');

  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

fakeDOMLoaded();

});
test('This test check if RatesCtrl called the class constructor', () => {
    //console.log("sss",json.results);
    let Rates = new RatesCtrl("body","rates",rates);
    expect(Rates.constructor.name).toBe("RatesCtrl");
});

test('This test check if methods RatesCtrl is inyected on DOM', () => {
    new RatesCtrl("body","rates",rates);
    let Rates = $("section#body").children.length;
    expect(Rates).toBeGreaterThan(1);
});


