import { responsiveMenu, generaTemplate, get, toaster, setCookie, getCookie } from '../src/utils/utils';
const $ = require('jquery');

beforeEach(() => {
    // Set up our document body
    document.body.innerHTML = `
  <nav class="top-nav" id="myTopnav">
      <a href="" class="top-nav__itemLogo"><img src="" id="header_logo" alt=""></a>
      <a href="#" id="home" class="top-nav__item top-nav__item--active" data-tr="Home"></a>
      <a href="#catalogue" id="contact" class="top-nav__item" data-tr="Catalogue"></a>
      <a href="#rates" id="contact" class="top-nav__item" data-tr="Rates"></a>
      <a href="#contact" id="contact" class="top-nav__item" data-tr="Contact"></a>
      <a href="#about" id="contact" class="top-nav__item" data-tr="About"></a>
      <a href="javascript:void(0);" class="top-nav__item" id="icon">
        <i class=""><img src="./media/responsive.svg" alt="responsive" height="16" width="16"></i>
      </a>
      <!--<input type="submit" value="Castellano" class="btn btn__info" id="btnCast">-->
      <div class="top-nav__languages">
        <div id="btnCast" class="top-nav__languagesSP"><img src="./media/spain.svg" alt="españa" width="20" height="20"></div>
        <div id="btnVal"><img src="./media/valencia.png" alt="valencia" width="20" height="20"></div>
      </div>
      
      <!--<input type="submit" value="Valencia" class="btn btn__info" id="btnVal">-->
    </nav>
    <div id="errorName" class=""></div>
  `;

});

const mockXHR = {
    open: jest.fn(),
    send: jest.fn(),
    onload: jest.fn(),
    onerror: jest.fn(),
    status: 200,
    response: JSON.stringify(
        [
            { title: 'test post' },
            { title: 'second test post' }
        ]
    ),
    statusText: 'Server not ready'

};
const oldXMLHttpRequest = window.XMLHttpRequest;
window.XMLHttpRequest = jest.fn(() => mockXHR);

it('Should retrieve a list of posts from the server when calling get', function (done) {
    const reqPromise = get('posts');
    mockXHR.onload();
    console.log("GET", reqPromise);
    reqPromise.then((posts) => {
        posts = JSON.parse(posts);
        expect(posts.length).toBe(2);
        expect(posts[0].title).toBe('test post');
        expect(posts[1].title).toBe('second test post');
        done();
    });
});

test('We check that generaTemplate returns elements html and content from an object like parametrer', () => {
    let data = { title: "TESTING" };
    const form = generaTemplate`<h1 class="test">${'title'}</h1>`(data);
    expect(form).toBe('<h1 class="test">TESTING</h1>');

});

test('This test check that when screen it have 600px(fake function responsiveMenu), the menu is responsive, then class change top-nav responsive', () => {
    responsiveMenu();
    let classTopNav = $(".top-nav").attr("class");
    expect(classTopNav).toBe("top-nav responsive");
});

test('check toaster', () => {
    let errorName = document.getElementById('errorName');
    toaster(errorName, "errors", "joan");
    let error = $("#errorName").attr("class");
    expect(error).toBe("toaster toaster-errors");
    let valueError = $("#errorName").html();
    expect(valueError).toBe("joan");

});

test('check cookies', () => {
    setCookie("testing", "true");
    let cookie = getCookie("testing");
    expect(cookie).toBe("true");
});
