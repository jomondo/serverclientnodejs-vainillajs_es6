import CatalogueCtrl from '../src/Components/catalogue/catalogueCtrl';
import {catalogue,familia} from "./fakeData/catalogue";
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
    document.body.innerHTML =`
    <section id="body">
    
    </section>
  `;

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');

  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

fakeDOMLoaded();

});
test('This test check if CatalogueCtrl called the class constructor', () => {
    //console.log("sss",json.results);
    let catalogues = new CatalogueCtrl("body","catalogue",catalogue,familia[0]);
    expect(catalogues.constructor.name).toBe("CatalogueCtrl");
});

test('This test check if methods CatalogueCtrl is inyected on DOM', () => {
    new CatalogueCtrl("body","catalogue",catalogue,familia[0]);
    let catalogues = $("section#body").children.length;
    expect(catalogues).toBeGreaterThan(1);
});


