import {head,header,footer} from '../src/layout/layout';
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
    document.body.innerHTML =`
    <head></head>
    <header>
    <nav class="top-nav" id="myTopnav">
      <a href="" class="top-nav__itemLogo"><img src="" id="header_logo" alt=""></a>
      <a href="#" id="home" class="top-nav__item top-nav__item--active" data-tr="Inicio">Home</a>
      <a href="#contact" id="contact" class="top-nav__item" data-tr="Contacto">Contacto</a>
      <a href="#about" id="contact" class="top-nav__item" data-tr="Contacto">About</a>
      <a href="javascript:void(0);" class="top-nav__item" id="icon">
        <i class=""><img src="./media/responsive.svg" alt="responsive" height="16" width="16"></i>
      </a>
      <input type="submit" value="Castellano" class="btn btn__info" id="btnCast">
      <input type="submit" value="Valencia" class="btn btn__info" id="btnVal">
    </nav>
  </header>
  <footer class="footer">
  <ul class="footer-item">
    <li class="footer-item__company"></li>
    <li class="footer-item__city"></li>
    <li class="footer-item__zipCode"></li>
    <li class="footer-item__phone"></li>
  </ul>
  <ul class="footer-item">
    <li>
      <a class="footer-social__facebook" href="">
        <img src="./media/facebook.svg" height="30" width="30">
      </a>
      <a class="footer-social__twitter" href="">
        <img src="./media/twitter.svg" height="30" width="30">
      </a>
      <a href="#cookies" id="cookies" class="top-nav__item" data-tr="Cookies">Cookies</a>
      <a href="#aviso-legal" id="aviso-legal" class="top-nav__item" data-tr="Aviso Legal">Aviso legal</a>
      <a href="#about" id="about" class="top-nav__item" data-tr="About">Sobre nosotros</a>
      <a href="#contact" id="contact" class="top-nav__item" data-tr="Contact">Contact</a>
    </li>
  </ul>
</footer>
  `;

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');

  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

fakeDOMLoaded();

});

test('Testing shortcut icon, it have create a link element inside head element', () => {
    let data = {logo:"http://localhost:8000/media/logo/mobile-broadband-modem.svg"};
    head(data);
    let elementHead = $("head").html();
    expect(elementHead).toBe('<link rel="shortcut icon" type="image/png" href="'+data.logo+'">');
});

test('Testing if logotip is inyected on img element (attribute src)', () => {
    let data = {logo:"http://localhost:8000/media/logo/mobile-broadband-modem.svg", name:"Siempre Online"};
    header(data);
    let elementSrcImg = $("img#header_logo").attr("src");
    expect(elementSrcImg).toBe(data.logo);
});

test('Testing footer (items datos empresa and href network social) ', () => {
    let data = {name:"Siempre Online", city:"Ontiyent", zipcode:"46870",phone:"625070506",facebook:"testFacebook",twitter:"testTwitter"};
    footer(data);
    let footerName = $(".footer-item__company").html();
    expect(footerName).toBe(data.name);
    let footerCity = $(".footer-item__city").html();
    expect(footerCity).toBe(data.city);
    let footerCode = $(".footer-item__zipCode").html();
    expect(footerCode).toBe(data.zipcode);
    let footerPhone = $(".footer-item__phone").html();
    expect(footerPhone).toBe(data.phone);
    let footerFacebook = $(".footer-social__facebook").attr("href");
    expect(footerFacebook).toBe(data.facebook);
    let footerTwitter = $(".footer-social__twitter").attr("href");
    expect(footerTwitter).toBe(data.twitter);

});

