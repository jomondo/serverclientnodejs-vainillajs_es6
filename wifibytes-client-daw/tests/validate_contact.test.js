const contact=require('../src/Components/contact/validateContact');
const $ = require('jquery');

beforeEach(()=> {
    // Set up our document body
  document.body.innerHTML =`
  <section style="float:left;" id="body">
    <form id="form-contact">
      <label for="name">Name:</label><br>
      <input type="text" id="name"><br>
      <span id="errorName" class="errors"></span><br>
      <label for="email">Email:</label><br>
      <input type="email" id="email"><br>
      <span id="errorEmail" class="errors"></span><br>
      <label for="subject">Subject:</label><br>
      <input type="text" id="subject"><br>
      <span id="errorSubject" class="errors"></span><br>
      <label for="comment">Comment:</label><br>
      <textarea name="comment" id="comment" cols="30" rows="10"></textarea><br>
      <span id="errorComment" class="errors"></span><br>
      <button type="button" id="submitContact" class="btn-contact">Submit</button>
    </form>
  </section>
  `;

function fakeDOMLoaded() {
  const fakeEvent = document.createEvent('Event');

  fakeEvent.initEvent('DOMContentLoaded', true, true);
  window.document.dispatchEvent(fakeEvent);
}

fakeDOMLoaded();

});

test('Check validation contact form toBe true', () => {
  $("#name").val("jomondo");
  $("#email").val("joanmodaw@gmail.com");
  $("#subject").val("jomondo");
  $("#comment").val("jomondocomment");
  
  expect(contact.validateContact()).toBe(true);
  
});

test('Check validation contact form toBe false (email incorrect)', () => {
  $("#name").val("jomondo");
  $("#email").val("joanmodawgmail.com");
  $("#subject").val("jomondo");
  $("#comment").val("jomondocomment");
  expect(contact.validateContact()).toBe(false);
  
 /* expect($('#about').length).toBeGreaterThan(0); */ 
  //expect($('#selectFunction option').length).toBeGreaterThan(0);   
});
