INTRODUCTION
=============

This silly project is only an excuse to explain the basic scaffolding of a SPA web application.
The project is programmed in raw ES6 (ES2015) and transpiled, packaged, uglified and served using as a supporting tools; webpack, babel, webpack-dev-server, jsdoc ...
Could be used as a base to develop any other javascript project during the course

Key points 
===========

* Unit battery testing with jest
* Hot live reloading with webpack hmr
* PAckaged with webpack
* Transpiled with babel
* Documented with jsdoc

#Webpack

NICE TO READ https://www.valentinog.com/blog/webpack-tutorial/

#Ajax and fetch API 
NICE TO READ: https://dev.to/bjhaid_93/beginners-guide-to-fetching-data-with-ajax-fetch-api--asyncawait-3m1l

TIPS
=====

To solve some jest testing problems I've been forced to install

npm install babel-core@7.0.0-bridge.0 --save-dev

Functionalities
================

- home and contact
- Routing system
- carousel
- facebook and twitter in footer or header
- legal notice, cookies, contact, about
- test with jest
- documentation with jsdoc
- get tarifas
- i18n [in this moment 2 idioms -> spanish and catalan, get and set cookies]
- refactoring component using inheritance
- Cataleg
- Rates

Problems that I have had and observations
==========================================

- At the time of using inheritance, I did not use classes, so I had to rethink change in all controllers, why? because following the same path using functions meant using prototypes, and these are from ES5, and if we are doing it with ES6 it is better to use it and use classes, so I had to do more work (renew documentation, fix many tests and reorganize everything again). Therefore, I think it is very important before you start programming, think about the structure you are going to have, how to organize it in such a way that it does not prevent the web application from growing, if your application is going to be a small, medium or large project, if use classes or not, among others ...

Improvements
=============

- In the get function of utils, we clean the cache after 5 minutes so that it returns the updated backend data again. This way, we save many requests to the server and make sure that the user has updated data every 5 minutes.
- Toaster is used to inform the user (sucess,errors,info)
- We use a template system, which I pass the id where I want to inject it and the template I want to use, this way we have all the templates in 1 folder, which are called from a single script, except the subtemplates, which will be dynamically injected and calls from loadContent of each class.

Web Design
===========

- We use meta on head
- we use BEM methodology
- robots.txt and humans.txt created (inside the root folder ./www/)
- We use elements HTML5
- All templates are located in the folder called templates, except the head, header, nav, and footer that are in ./www/index.html
- The css is found on ./www/css/main.css
